# Taking Tickets
This document aims to provide useful information for me while I am in ticket-taking mode.

## GitLab info
  * [GitLab releases](https://about.gitlab.com/releases/) - This gives a high-level set of release notes per release and broken down by tier (Premium, Ultimate, etc). 
  * [CHANGELOG.md](https://gitlab.com/gitlab-org/gitlab/blob/master/CHANGELOG.md)
