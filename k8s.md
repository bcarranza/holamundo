# Learning K8s
## minikube

  1. Install [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
  2. Switch to the Bitnami [getting started on Kubernetes](https://docs.bitnami.com/kubernetes/get-started-kubernetes/) guide for the rest.

When done with both, you'll have:

  - minikube
  - k8s
  - helm

https://docs.bitnami.com/kubernetes/get-started-kubernetes/

## Deploying GitLab
**TODO** Look through the GitLab deployment guide to prepare your environment.

GitLab Helm Chart Docs > [Deployment Guide](https://docs.gitlab.com/charts/installation/deployment.html)

## Environment
> Brief notes about the environment that these tests were performed in. 
I'm mostly tesing `minikube` with the `docker` driver on macOS Catalina. 

```
minikube start --driver=docker
```                                                                                                                                                                    
## Useful Commands
```
minikube start --driver=docker
```

`kubectl describe node` so much info!
