# holamundo
This document contains my notes on what I learned or put together to make things easier while working at GitLab. 

# GitLab
This section is full of GitLab-specific stuff. There is another section that contains more general stuff. Note: there will be overlap. 
## Additional Search Engines
  - View a ticket: `https://gitlab.zendesk.com/agent/tickets/%s`
  - Print a ticket: `https://gitlab.zendesk.com/tickets/%s/print`


## Droplets
What are some packages you wind up installing frequently in your DO droplet? Consider assembling an Ansible playbook.

## GitLab Administration
Here are some useful links:

  - [Available Rake Tasks](https://docs.gitlab.com/ee/raketasks/)

```sudo gitlab-rake gitlab:env:info```

### Packages
#### Diagnostics
  - `net-tools` - to get netstat
#### GitLab Prerequisites
These are packages that are required for various GitLab things.

  - `libicu-dev` - For [ElasticSearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html).

## Configuring GitLab
A one-liner to update the `external_url` value:

```
sed -i "s/^external_url 'http:\/\/gitlab.example.com'/external_url 'https:\/\/troubleshooting.party'/g" /etc/gitlab/gitlab.rb
```

## Administering GitLab
### Rails Console
List the id of every project in the GitLab instance:

```
projects = Project.all()
projects.each do |p|
     puts "Project ID: #{p.id}"
end
```

## Backing up GitLab
### Managing Backups
This one liner will look through by `~/gitlab-backups/` directory and tell me which versions of GitLab I have backups for:

```
find backups -type f -iname \*gitlab_backup.tar | cut -d"/" -f2 | cut -d"_" -f5 | sort | uniq
```

# General
## ElasticSearch
It's accessible to `localhost:9200` by default. 

Creating an index (when `gitlab-rake` tasks are not available to you because you are following best practices and running ElasticSearch on another node):

```
curl -X PUT "localhost:9200/gitlab?pretty"
```

### View all ES indices
```
curl -X GET "localhost:9200/_cat/indices/*?v&s=index&pretty"
```
If you are not running Elasticsearch on the same host that GitLab is running on, swap in the IP addr for the host that GitLab is running on. Make sure that this works before enabling Elasticsearch as an integration for GitLab. 


## macOS
I am used to running something like `firefox --ProfileManager &` to access Firefox's profile manager. (Many Web browsers support this functionality and I highly recommend it to folks doing most forms of tech support. Especially now that Web browsers are eating the world.)

To access the Firefox Profile Manager in macOS:

```
/Applications/Firefox.app/Contents/MacOS/firefox-bin -profilemanager
```

I unchecked `Use the selected profile without asking at startup` so that I can forget this trick until I reinstall Firefox or the OS. 

[Mozilla Documentation](http://kb.mozillazine.org/Profile_Manager#Accessing_the_Profile_Manager)

### Use this, not that
Use `md5` instead of `md5sum`

## Emacs
How do I insert a current timestamp?

See [the answer](https://stackoverflow.com/questions/11295973/how-to-insert-current-time-in-the-emacs-org-mode) in full on SO.

Hint:

> C-u C-c .

This will bring you to a prompt to select a date. Press `Enter` to select the current date and time.

