# Once Asked Questions | GitLab

> Unlike frequently asked questions, this documented is intended to capture questions I asked once and am unlikely to ask regularly. 


## CI
### Q: If you specify a list of custom stages and don't specify a stage for a job, what stage will that job wind up in?

Example:
```
stages:
  - cautious
  - optimistics

hello world:
  script:
    - echo "Hello, there."
```

**A:** Errors will be found in your `.gitlab-ci.yml`. It will be to the tune of:

```
Found errors in your .gitlab-ci.yml:
hello world job: chosen stage does not exist; available stages are .pre
cautious
optimistics
.post
```

Review how I tested this using the [chosen-stage-does-not-exist](https://gitlab.com/bcarranza/holamundo/-/tags/chosen-stage-does-not-exist) tag. 