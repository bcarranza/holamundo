# Useful Links

Looking for more info about something in `/opt/gitlab/embedded/service/gitlab-rails/ee`, check out [https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee).

## Git Commands

  - You will learn that `git blame` is your friend. 

## Articles

  - [How to use git blame effectively](https://gitbetter.substack.com/p/how-to-use-git-blame-effectively)
